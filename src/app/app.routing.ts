import { Routes } from '@angular/router';


import { WelcomeRouting }  from './welcome/welcome.routing'
import { SimulationRouting }  from './simulation/simulation.routing'


export const AppRouting: Routes= [
  ...WelcomeRouting,
  ...SimulationRouting,
  { path: '*', redirectTo:'/',pathMatch:'full'}

]