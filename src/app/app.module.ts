import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
/*
************************************************
*    Material modules for app
*************************************************
*/
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule  } from "./app.material";
import 'hammerjs';
/*
************************************************
*     principal component
*************************************************
*/
import { AppComponent } from './app.component';

/*
************************************************
*     modules of  your app
*************************************************
*/

import { WelcomeModule } from "./welcome/welcome.module";
import { SimulationModule } from "./simulation/simulation.module";

/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/
import { AppRouting } from './app.routing';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule.forRoot(AppRouting,{ useHash: true }),
    WelcomeModule,
    SimulationModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
