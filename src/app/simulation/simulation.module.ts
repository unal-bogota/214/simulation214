import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
/*
************************************************
*    Material modules for app
*************************************************
*/
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule  } from "../app.material";
import 'hammerjs';
/*
************************************************
*     modules of  your app
*************************************************
*/

import { CoffeeSimulationModule } from './coffee-simulation/coffee-simulation.module';

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
***/

/*
************************************************
*     principal component
*************************************************
*/
import { SimulationComponent } from './simulation/simulation.component';
import { ParametersComponent } from './parameters/parameters.component';
import { ChartsComponent } from './charts/charts.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    CoffeeSimulationModule,
  ],
  
  declarations: [SimulationComponent, ParametersComponent, ChartsComponent]
})
export class SimulationModule { }
