import { Routes } from '@angular/router';


import { SimulationComponent }  from './simulation/simulation.component'

import { CoffeeSimulationRouting } from './coffee-simulation/coffee-simulation-routing';

export const SimulationRouting: Routes= [
  { path:'simulations', component: SimulationComponent,
  children: [
    { path: '', redirectTo: 'coffee', pathMatch: 'full'},
    { path: 'data', component: SimulationComponent},
    ...CoffeeSimulationRouting
    
  ]},
  
]

