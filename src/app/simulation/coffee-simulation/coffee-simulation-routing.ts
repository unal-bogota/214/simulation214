import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/

import { IndexComponent } from './index/index.component';
import { FixedLayerComponent } from './fixed-layer/fixed-layer.component';
import { IfcComponent } from './ifc/ifc.component';


export const CoffeeSimulationRouting: Routes= [
  { path:'coffee', component: null,
  children: [
    { path: '', redirectTo: 'index', pathMatch: 'full'},
    { path: 'index', component: IndexComponent},
    { path: 'fixed-layer', component: FixedLayerComponent},
    { path: 'ifc', component: IfcComponent},
    
    
  ]}
]

