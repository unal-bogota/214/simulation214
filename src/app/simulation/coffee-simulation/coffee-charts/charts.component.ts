import { Component, OnInit,Input,Output } from '@angular/core';

@Component({
  selector: 'app-coffee-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class CoffeeChartsComponent implements OnInit {
  // Attribute
  hide=[true,true]
  /** Es Visible el gráfico */
  @Input()
  showChart:boolean

  /* Enrada de Datos a Graficas Eje [Y] */
  @Input()
  public lineChartData:Array<any>
  /* Enrada de Datos a Graficas Eje [X] */
  @Input()
  public lineChartLabels:Array<any>

  public lineChartColors:Array<any> = [
    
    { // grey
      //backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      //backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      //backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  //Linea
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';


  
  public lineChartOptions:any = {
    responsive: true
  };

  constructor() { 

    if(this.showChart!=undefined ||this.showChart!=null ){
      this.hide[0]=this.showChart
    }else{
      this.hide[0]=true
    }
  }

  ngOnInit() {
    console.log("DATOS --> ", this.lineChartData)

  }



  public randomize():void {
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }


    // events
    public chartClicked(e:any):void {
      console.log(e);
    }
   
    public chartHovered(e:any):void {
      console.log(e);
    }

}
