import { Component, OnInit,Input,Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-ifc',
  templateUrl: './ifc.component.html',
  styleUrls: ['./ifc.component.css']
})
export class IfcComponent implements OnInit {
    // Flag
    showParameter=true
    showChart=true
    showTable=true
    showResults=false


    /**=======================
    *  Datos de Gráfico
    */
    public lineChartData:Array<any> 
  
    /* Etiquetas del Eje X */
    public lineChartLabels:Array<any> 
    
    /**=======================
     * Datos de Tabla
     */
     /** Etiquetas de Columnas*/
    displayedColumns

    // Origen de los datos a la Tabla
    dataSource

  constructor() { }

  ngOnInit() {
  }
  
  /* Parametros de entrada del formulario */
  handleParameterEmit(parameters){
    console.log("PARAMETROS COMPARTIDOS ",parameters)

    // inicia los respectivos calculos de la simulación
    this.processParameters()

    
  }

  /**Start Simulation */
  processParameters(){

    // Llama la creación del proceso de 
    // creación de datos para la visualización
    this.processResult()

  }/**End Simulation*/


  processResult(){

    this.convertToChart()
    this.convertToTable()
  

    this.showChart=true
    this.showTable=true
    this.showResults=true
  }

 

  convertToChart(){
    console.log("DATOS PARA GRAFICAR")
    // Valores
    this.lineChartData=[
      {data:[45,54,45,30,26, 23], label:"Secado # 1"},
      {data:[56,51,49,34,29, 23], label:"Secado # 2"}
    ];

    // Etiquetas
    this.lineChartLabels=[
      '1 h','2 h','3 h','4 h','5 h','6 h'
    ]
  }

  convertToTable(){
    this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
    this.dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);

  }

}
export interface Element {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
const ELEMENT_DATA: Element[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
  { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
  { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
  { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
  { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
  { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
  { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
  { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
  { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
  { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];
