export class ParameterIFC {
  formProducto?: FormProducto
  formLugar?: FormLugar
  formAire?: FormAire
  formSistema?: FormSistema
  formLongitud?: FormLongitud


  constructor(  formProducto?: FormProducto,
    formLugar?: FormLugar,
    formAire?: FormAire,
    formSistema?: FormSistema,
    formLogitud?: FormLongitud) {

  }

}
export class FormProducto {
  humedad_inicial?: Number
  temperatura_inicial?: Number
  hum_final_prom?: Number
  flujo_cafe?: Number

  constructor(humedad_inicial?: Number,
    temperatura_inicial?: Number,
    hum_final_prom?: Number,
    flujo_cafe?: Number) { }
}

export class FormLugar {
  altitud?: Number
  temperatura_ambiente?: Number

  constructor(altitud?: Number,
    temperatura_ambiente?: Number) { }
}

export class FormAire {
  temperatura?: Number
  humedad_relativa?: Number
  caudal?: Number

  constructor(temperatura?: Number,
    humedad_relativa?: Number,
    caudal?: Number) { }
}

export class FormSistema {
  seccion_transversal?: Number
  altura_columna?: Number
  imprimir_resultados?: Number

  constructor(seccion_transversal?: Number,
    altura_columna?: Number,
    imprimir_resultados?: Number) { }
}

export class FormLongitud {
  estimar_longitud?: Number
  valor_indice?: Number
  valor_longitud?: Number

  constructor(estimar_longitud?: Number,
    valor_indice?: Number,
    valor_longitud?: Number) { }


}