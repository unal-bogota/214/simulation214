import { Component, OnInit, Output, EventEmitter,Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MatDialog } from '@angular/material';
/*
************************************************
*     other components
*************************************************
*/
import { ParameterDialogIfcComponent } from '../parameter-dialog-ifc/parameter-dialog-ifc.component';
/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/
import { ParameterIFC, FormAire, FormLongitud, FormLugar, FormProducto, FormSistema } from '../parameterIFC'
/*
************************************************
*     constant of  your app
*************************************************
*/

@Component({
  selector: 'app-parameter-ifc',
  templateUrl: './parameter-ifc.component.html',
  styleUrls: ['./parameter-ifc.component.css']
})
export class ParameterIfcComponent implements OnInit {
  // refences form objects
  formProducto: FormGroup;
  formLugar: FormGroup;
  formAire: FormGroup;
  formSistema: FormGroup;
  formLongitud: FormGroup;



  @Output() ParameterEmit = new EventEmitter();

  // attibutes
  hide = [true, true, true, true, true, true, true, true, true, true, true];

  caseSI = true // flag para controlar que tipo de pregunta realizar

  @Input()
  showParameter:boolean

  //references from objects
  parameter: ParameterIFC;
  producto: FormProducto;
  aire: FormAire;
  longitud: FormLongitud;
  lugar: FormLugar;
  sistema: FormSistema;

  constructor(private fb: FormBuilder, private router: Router, public dialog: MatDialog) {
    this.producto = new FormProducto()
    this.lugar = new FormLugar()
    this.aire = new FormAire()
    this.sistema = new FormSistema()
    this.longitud = new FormLongitud()
    this.parameter = new ParameterIFC()

    this.createControl()
    this.loadData()
    this.changeForm()

    if(this.showParameter!=undefined ||this.showParameter!=null ){
      this.hide[5]=this.showParameter
    }else{
      this.hide[5]=true
    }

  }

  // Method of component 
  /*
  @You can't remove this method
  */
  ngOnInit() {
  }

  /* Load of Dialog */
  openParameterDialog() {

    const dialogRef = this.dialog.open(ParameterDialogIfcComponent, {
      width: '100%',
    });

    dialogRef.afterClosed().subscribe(result => {
      //Recivimos los datos que comparte el popup, una vez sea cerrado
      console.log("RESPUESTA: ", result);

    });
  }

  //crea los controles del formulario WEB
  createControl() {
    this.formProducto = this.fb.group({
      humedad_inicial: ['', Validators.compose([
        Validators.required
      ])],
      temperatura_inicial: ['', Validators.compose([
        Validators.required
      ])],
      hum_final_prom: ['', Validators.compose([
        Validators.required
      ])],
      flujo_cafe: ['', Validators.compose([
        Validators.required
      ])],

    });

    this.formLugar = this.fb.group({
      altitud: ['', Validators.compose([
        Validators.required
      ])],
      temperatura_ambiente: ['', Validators.compose([
        Validators.required
      ])],
    });
    this.formAire = this.fb.group({
      temperatura: ['', Validators.compose([
        Validators.required
      ])],
      humedad_relativa: ['', Validators.compose([
        Validators.required
      ])],
      caudal: ['', Validators.compose([
        Validators.required
      ])],
    });

    this.formSistema = this.fb.group({
      seccion_transversal: ['', Validators.compose([
        Validators.required
      ])],
      altura_columna: ['', Validators.compose([
        Validators.required
      ])],
      imprimir_resultados: ['', Validators.compose([
        Validators.required
      ])],

    });
    this.formLongitud = this.fb.group({
      estimar_longitud: ['', Validators.compose([
        Validators.required
      ])],
      valor_indice: ['', Validators.compose([
        Validators.required
      ])],
      valor_longitud: ['', Validators.compose([
        Validators.required
      ])],

    });

  }
  /* Carga de datos iniciales en el formulario */
  loadData() {
    this.formProducto.patchValue({
      humedad_inicial: "54",
      temperatura_inicial: "23",
      hum_final_prom: "11",
      flujo_cafe: "800"
    })

    this.formLugar.patchValue({
      altitud: "1310",
      temperatura_ambiente: "21",
    })
    this.formAire.patchValue({
      temperatura: "80",
      humedad_relativa: "0.029",
      caudal: "50",
    })
    this.formSistema.patchValue({
      seccion_transversal: "1",
      altura_columna: "0.65",
      imprimir_resultados: "1",
    })
    this.formLongitud.patchValue({
      estimar_longitud: "1",
      valor_indice: "",
      valor_longitud: "",
    })

  }
  /**
   * Administrador de cambios en el formulario.
   * Listener, que se activa una vez suceda un cambio en el formulario
   */
  changeForm() {

  }

  /**========================================================== */
  // Transformar los datos del formularios en objeto de parametros

  save() {
    // Oculto formulario
    this.hide[5]=false

    // Capturando Datos de producto del formulario
    this.producto.humedad_inicial = this.formProducto.value['humedad_inicial']
    this.producto.temperatura_inicial = this.formProducto.value['temperatura_inicial']
    this.producto.hum_final_prom = this.formProducto.value['hum_final_prom']
    this.producto.flujo_cafe = this.formProducto.value['flujo_cafe']

    // Capturando Datos de Lugar del formulario

    this.lugar.altitud = this.formLugar.value['altitud']
    this.lugar.temperatura_ambiente = this.formLugar.value['temperatura_ambiente']

    // Capturando Datos de Aire del formulario

    this.aire.temperatura = this.formAire.value['temperatura']
    this.aire.humedad_relativa = this.formAire.value['humedad_relativa']
    this.aire.caudal = this.formAire.value['caudal']

    // Capturando Datos de Sistema del formulario

    this.sistema.seccion_transversal = this.formSistema.value['seccion_transversal']
    this.sistema.altura_columna = this.formSistema.value['altura_columna']
    this.sistema.imprimir_resultados = this.formSistema.value['imprimir_resultados']

    // Capturando Datos de Longitud del formulario

    this.longitud.estimar_longitud = this.formLongitud.value['estimar_longitud']
    this.longitud.valor_indice = this.formLongitud.value['valor_indice']
    this.longitud.valor_longitud = this.formLongitud.value['valor_longitud']


    this.parameter.formProducto = this.producto
    this.parameter.formLugar = this.lugar
    this.parameter.formAire = this.aire
    this.parameter.formSistema = this.sistema
    this.parameter.formLongitud = this.longitud

    console.log("PARAMETROS CAPTURADOS ", this.parameter);
    this.ParameterEmit.emit(this.parameter)
  }
}


