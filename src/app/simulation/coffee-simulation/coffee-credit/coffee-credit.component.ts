import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/***    import from Angular Material */
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material'

@Component({
  selector: 'app-coffee-credit',
  templateUrl: './coffee-credit.component.html',
  styleUrls: ['./coffee-credit.component.css']
})
export class CoffeeCreditComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CoffeeCreditComponent>) { }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
