export class ParameterFL {

    formAire?:FormAire
    formProducto?:FormProducto
    formLugar?:FormLugar
    formSistema?:FormSistema
    formSecador?:FormSecador
    formFlujoSecador?:FormFlujoSecador
    formFlujoAire?:FormFlujoAire


    constructor(formAire?:FormAire,
        formProducto?:FormProducto,
        formLugar?:FormLugar,
        formSistema?:FormSistema,
        formSecador?:FormSecador,
        formFlujoSecador?:FormFlujoSecador,
        formFlujoAire?:FormFlujoAire) {

    }

}

export class FormAire {
    temperatura?: Number
    humedad_relativa?: Number
    caudal?: Number

    constructor(temperatura?: Number,
        humedad_relativa?: Number,
        caudal?: Number) { }
}

export class FormProducto {
    hum_final_prom?: Number
    humedad_inicial?: Number
    temperatura_inicial?: Number

    constructor(humedad_inicial?: Number,
        temperatura_inicial?: Number,
        hum_final_prom?: Number) { }
}

export class FormLugar {
    altitud?: Number
    temperatura_ambiente?: Number

    constructor(altitud?: Number,
        temperatura_ambiente?: Number) { }
}



export class FormSistema {
    seccion_transversal?: Number
    espesor_capa?: Number
    imprimir_resultados?: Number

    constructor(seccion_transversal?: Number,
        espesor_capa?: Number,
        imprimir_resultados?: Number) { }
}

export class FormSecador {
    tipo_secador?: Number
    constructor(tipo_secador?: Number) { }
}
export class FormFlujoSecador {
    invertir_aire?: Number
    invertir_hora?:Number
    constructor(invertir_aire?: Number,
        invertir_hora?:Number) { }
}
export class FormFlujoAire {
    sentido_aire?: Number
    constructor(sentido_aire?: Number) { }
}