import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MatDialog } from '@angular/material';
/*
************************************************
*     other components
*************************************************
*/
import { ParameterDialogFlComponent } from '../parameter-dialog-fl/parameter-dialog-fl.component';
/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/
import { ParameterFL, FormAire, FormProducto, FormLugar, FormSistema, FormFlujoSecador, FormSecador, FormFlujoAire } from './parameterFL'
/*
************************************************
*     constant of  your app
*************************************************
*/

@Component({
  selector: 'app-parameter-fl',
  templateUrl: './parameter-fl.component.html',
  styleUrls: ['./parameter-fl.component.css']
})
export class ParameterFlComponent implements OnInit {
  // refences form objects
  formAire: FormGroup;
  formProducto: FormGroup;
  formLugar: FormGroup;
  formSistema: FormGroup;
  formSecador: FormGroup;
  formFlujoSecador: FormGroup;
  formFlujoAire: FormGroup;






  // attibutes
  hide = [true, true, true, true, true, true, true, true, true, true, true];
  // Ctrl
  @Input() showParameter: boolean

  @Output() ParameterEmit = new EventEmitter();

  //references from objects
  parameter: ParameterFL;
  aire: FormAire;
  producto: FormProducto;
  lugar: FormLugar;
  sistema: FormSistema;
  secador: FormSecador;
  flujoSecador: FormFlujoSecador;
  flujoAire: FormFlujoAire;

  constructor(private fb: FormBuilder, private router: Router, public dialog: MatDialog) {
    this.parameter = new ParameterFL()
    this.aire = new FormAire()
    this.producto = new FormProducto()
    this.lugar = new FormLugar()
    this.sistema = new FormSistema()
    this.secador = new FormSecador();
    this.flujoSecador = new FormFlujoSecador();
    this.flujoAire = new FormFlujoAire();


    this.createControl()
    this.loadData()
    this.changeForm()

    if (this.showParameter != undefined || this.showParameter != null) {
      this.hide[7] = this.showParameter
    } else {
      this.hide[7] = true
    }


  }

  ngOnInit() {
  }

  /* Load of Dialog */
  openParameterDialog() {

    const dialogRef = this.dialog.open(ParameterDialogFlComponent, {
      width: '100%',
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  /** Begin Confg Form */
  createControl() {
    this.formAire = this.fb.group({
      temperatura: ['', Validators.compose([
        Validators.required
      ])],
      humedad_relativa: ['', Validators.compose([
        Validators.required
      ])],
      caudal: ['', Validators.compose([
        Validators.required
      ])],
    });

    this.formProducto = this.fb.group({
      hum_final_prom: ['', Validators.compose([
        Validators.required
      ])],
      humedad_inicial: ['', Validators.compose([
        Validators.required
      ])],
      temperatura_inicial: ['', Validators.compose([
        Validators.required
      ])],
    });

    this.formLugar = this.fb.group({
      altitud: ['', Validators.compose([
        Validators.required
      ])],
      temperatura_ambiente: ['', Validators.compose([
        Validators.required
      ])],
    });


    this.formSistema = this.fb.group({
      seccion_transversal: ['', Validators.compose([
        Validators.required
      ])],
      espesor_capa: ['', Validators.compose([
        Validators.required
      ])],
      imprimir_resultados: ['', Validators.compose([
        Validators.required
      ])],

    });

    this.formSecador = this.fb.group({
      tipo_secador: ['', Validators.compose([
        Validators.required
      ])]
    });
    this.formFlujoSecador = this.fb.group({
      invertir_aire: ['', Validators.compose([
        Validators.required
      ])],
      invertir_hora: ['', Validators.compose([
        Validators.required
      ])]
    });
    this.formFlujoAire = this.fb.group({
      sentido_aire: ['', Validators.compose([
        Validators.required
      ])]
    });

  }/** End Confg Form */

  // carga de datos en los formularios
  loadData() {
    this.formAire.patchValue({
      temperatura: "80",
      humedad_relativa: "0.029",
      caudal: "50",
    })

    this.formProducto.patchValue({
      hum_final_prom: "11",
      humedad_inicial: "54",
      temperatura_inicial: "23"
    })

    this.formLugar.patchValue({
      altitud: "1310",
      temperatura_ambiente: "21",
    })

    this.formSistema.patchValue({
      seccion_transversal: "1",
      espesor_capa: "0.65",
      imprimir_resultados: "1",
    })

    this.formSecador.patchValue({
      tipo_secador: "1"
    });
    this.formFlujoSecador.patchValue({
      invertir_aire: "1",
      invertir_hora: "2"
    });
    this.formFlujoAire.patchValue({
      sentido_aire: "1"
    });

  }
  // control de cambios de los formularios
  changeForm() {

    this.formSecador.get('tipo_secador').valueChanges.subscribe(value=>{
      console.log("Valor de Tipo de Secado -|-|-> ",value);
      this.openParameterDialog()
    })

  }

  save() {
    // Oculto formulario
    this.hide[7] = false

    // Capturando Datos de producto del formulario
    this.producto.humedad_inicial = this.formProducto.value['humedad_inicial']
    this.producto.temperatura_inicial = this.formProducto.value['temperatura_inicial']
    this.producto.hum_final_prom = this.formProducto.value['hum_final_prom']


    // Capturando Datos de Lugar del formulario

    this.lugar.altitud = this.formLugar.value['altitud']
    this.lugar.temperatura_ambiente = this.formLugar.value['temperatura_ambiente']

    // Capturando Datos de Aire del formulario

    this.aire.temperatura = this.formAire.value['temperatura']
    this.aire.humedad_relativa = this.formAire.value['humedad_relativa']
    this.aire.caudal = this.formAire.value['caudal']

    // Capturando Datos de Sistema del formulario

    this.sistema.seccion_transversal = this.formSistema.value['seccion_transversal']
    this.sistema.espesor_capa = this.formSistema.value['espesor_capa']
    this.sistema.imprimir_resultados = this.formSistema.value['imprimir_resultados']

    // Capturando Datos de Secador del formulario
    this.secador.tipo_secador = this.formSecador.value['tipo_secador']
    // Capturando Datos de flujoSecador del formulario
    this.flujoSecador.invertir_aire = this.formFlujoSecador.value['invertir_aire']
    this.flujoSecador.invertir_hora = this.formFlujoSecador.value['invertir_hora']
    // Capturando Datos de flujoAire del formulario
    this.flujoAire.sentido_aire = this.formFlujoAire.value['sentido_aire']





    this.parameter.formProducto = this.producto
    this.parameter.formLugar = this.lugar
    this.parameter.formAire = this.aire
    this.parameter.formSistema = this.sistema
    this.parameter.formSecador = this.secador
    this.parameter.formFlujoSecador = this.flujoSecador
    this.parameter.formFlujoAire = this.flujoAire

    console.log("PARAMETROS CAPTURADOS ", this.parameter);
    this.ParameterEmit.emit(this.parameter)
  }


}
