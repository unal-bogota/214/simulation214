import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

/*
************************************************
*    Material modules for app
*************************************************
*/

import { MaterialModule  } from "../../app.material";
import 'hammerjs';

/*
************************************************
*     principal component
*************************************************
*/

import { IndexComponent } from './index/index.component';
import { FixedLayerComponent } from './fixed-layer/fixed-layer.component';
import { IfcComponent } from './ifc/ifc.component';
import { CoffeeChartsComponent } from './coffee-charts/charts.component';
import { DataTableComponent } from './data-table/data-table.component';

import { CoffeeCreditComponent } from './coffee-credit/coffee-credit.component';
import { ParameterFlComponent } from './fixed-layer/parameter-fl/parameter-fl.component';
import { ParameterIfcComponent } from './ifc/parameter-ifc/parameter-ifc.component';

import { ParameterDialogFlComponent } from './fixed-layer/parameter-dialog-fl/parameter-dialog-fl.component';
import { ParameterDialogIfcComponent } from './ifc/parameter-dialog-ifc/parameter-dialog-ifc.component';


/*
************************************************
*     modules of  your app
*************************************************
*/
import { SimulationModule } from '../simulation.module';

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
  ],
  entryComponents:[CoffeeCreditComponent,ParameterDialogFlComponent, ParameterDialogIfcComponent],
  declarations: [IndexComponent, FixedLayerComponent, IfcComponent, CoffeeCreditComponent, ParameterFlComponent,
     ParameterIfcComponent,CoffeeChartsComponent, ParameterDialogFlComponent, ParameterDialogIfcComponent,
     DataTableComponent]
})
export class CoffeeSimulationModule { }
