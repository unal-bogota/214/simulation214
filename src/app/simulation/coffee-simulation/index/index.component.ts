import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
/*
************************************************
*    Material modules for app
*************************************************
*/
import {MatDialog} from '@angular/material';
/*
************************************************
*     other components
*************************************************
*/
import {CoffeeCreditComponent} from '../coffee-credit/coffee-credit.component';
/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private router: Router,public dialog: MatDialog) { }

  ngOnInit() {
  }

  openCreditDialog(){
    
    const dialogRef = this.dialog.open(CoffeeCreditComponent, {
      width: '100%',
      });

      dialogRef.afterClosed().subscribe(result => {
      });
  }

  fixedLayer(){
    let link = ['/simulations/coffee/fixed-layer'];
    this.router.navigate(link);
  }

  ifc(){
    let link = ['/simulations/coffee/ifc'];
    this.router.navigate(link);
  }

}
