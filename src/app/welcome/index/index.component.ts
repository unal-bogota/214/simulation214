import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import {MatDialog} from '@angular/material';

import { ProductDialogComponent } from '../product-dialog/product-dialog.component';
import { LicenseComponent } from '../license/license.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  // attribute 
  product:any

  

  constructor(private router: Router, private route:ActivatedRoute ,public dialog: MatDialog ) { 

   
  }

  ngOnInit() {
 
  }
  license(){
    const dialogRef = this.dialog.open(LicenseComponent, {
      width: '100%',
      
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log("Dialog result: \n ",result);
        
        
      });
  }
  

 

  openProductDialog(){
    
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      width: '400px',
      data: { product: this.product }
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log("Dialog result: \n ",result);
        
        if(result && result!==""){
          this.product=result

          let link = ['/simulations/'+this.product.value];
          this.router.navigate(link);

        }
      });
    

  }

}
