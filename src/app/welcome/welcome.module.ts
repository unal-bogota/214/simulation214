import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule  } from "../app.material";
import 'hammerjs';
/*
************************************************
*     principal component
*************************************************
*/
import { IndexComponent } from './index/index.component';
import { HeadComponent } from './head/head.component';
import { ContentComponent } from './content/content.component';
import { TeamComponent } from './team/team.component';
import { ProductDialogComponent } from './product-dialog/product-dialog.component';
import { LicenseComponent } from './license/license.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule, RouterModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule, 
    
  ],
  entryComponents:[ProductDialogComponent,LicenseComponent],
  declarations: [IndexComponent, HeadComponent, ContentComponent, TeamComponent, ProductDialogComponent, LicenseComponent]
})
export class WelcomeModule { }
