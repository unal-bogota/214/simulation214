import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/***    import from Angular Material */
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material'

/****    import from others component  */

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.css']
})
export class ProductDialogComponent implements OnInit {

  // refences form objects
  form: FormGroup;
  // attibutes
  selectedValue: any;
  product: any;

  // Product Array
  products = [
    { value: 'coffee', viewValue: 'Café' },
    { value: 'cocoa', viewValue: 'Cacao' },
    { value: 'other', viewValue: 'Otro' }
  ];


  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<ProductDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.selectedValue = data.product
    
    
    this.product = data.product


    this.createControls()
    this.loadData()

  }

  ngOnInit() {
   
  }

  loadData() {
    let prodAux=this.products[0]
    if(this.product){
        if(this.product.value===this.products[0].value){
          prodAux=this.products[0]
        }else if(this.product.value===this.products[1].value){
          prodAux=this.products[1]
        }else if(this.product.value===this.products[2].value){
          prodAux=this.products[2]
        }
    }
    this.form.patchValue({
      product: prodAux
    })
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  agreeClick(): void {
    // If you select a option
    if (this.form.value['product'] && this.form.value['product'] !== null) {
      this.product = this.form.value['product']
      this.dialogRef.close(this.product);
    } else {// No have product
      this.dialogRef.close();
    }

  }

  createControls() {
    this.form = this.fb.group({
      product: ['', Validators.compose([
        Validators.required
      ])],

    });
  }


}
