const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')

let win

function createWindow () {
  win = new BrowserWindow({width: 800, height: 600})

  // load the dist folder from Angular
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'dist/index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools optionally:
  // win.webContents.openDevTools()

  win.on('closed', () => {
    win = null
  })
}

app.on('ready', createWindow)


app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})


/* const { app, BrowserWindow } = require('electron')

let win;

function createWindow(){

    win= new BrowserWindow({
        width:600,
        height:600,
        backgroundColor:'#ffffff',
        icon:`file://${__dirname}/dist/assets/logo.png`
    })

    win.loadURL(`file://${__dirname}/dist/index.html`)

    //// uncomment below to open the DevTools
    win.webContents.openDevTools()

    // Event when the windows is closed

    win.on('closed', function(){
        win=null
    })
}

// Create windows on electron initialization
app.on('ready', createWindow)

// Quit when all windows are closed
app.on('window-all-closed',function(){

    // On macOS specific close process
    if(process.platform !== 'darwin'){
        app.quit()
    }
})

app.on('activate', function(){
    // macOS specific close process
    if(win==null){
        createWindow()
    }
}) */